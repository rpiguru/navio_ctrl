"""
Test code for controlling ESC with Adafruit PCA9685 board.

The frequency is set as 45.5Hz, but real frequency is 50Hz.(Measured with Oscilloscope)

- At first, give 500 that ESC detects the PWM signal from the RPi.
If ESC does not detect input signal, its LED will blink and otherwise LED will be turned off.
- Set value to drive brushless motor.
In my case, motor begins moving from 325.

"""
import serial
import time
# Import the PCA9685 module.
import Adafruit_PCA9685
import xml.etree.ElementTree


class NavioCtrl:

    conf_file_name = ''
    serialPort = None
    pwm = None
    blds_channel = 0
    servo_channel = 1

    def __init__(self):

        self.conf_file_name = '/home/pi/navio/config.xml'
        self.serialPort = serial.Serial("/dev/ttyAMA0", 9600, timeout=0.5)

        # Initialise the PCA9685 using the default address (0x40).
        self.pwm = Adafruit_PCA9685.PCA9685()
        self.pwm.set_pwm_freq(float(self.get_param_from_xml('PWM_FREQ')))
        self.blds_channel = int(self.get_param_from_xml('BLDS_CHANNEL'))
        self.servo_channel = int(self.get_param_from_xml('SERVO_CHANNEL'))

    def set_param_to_xml(self, tag_name, new_val):
        et = xml.etree.ElementTree.parse(self.conf_file_name)
        for child_of_root in et.getroot():
            if child_of_root.tag == tag_name:
                child_of_root.text = new_val
                et.write(self.conf_file_name)
                return True
        return False

    def get_param_from_xml(self, param):
        """
        Get configuration parameters from the config.xml
        :param param: parameter name
        :return: if not exists, return None
        """
        root = xml.etree.ElementTree.parse(self.conf_file_name).getroot()
        tmp = None
        for child_of_root in root:
            if child_of_root.tag == param:
                tmp = child_of_root.text

        return tmp

    def get_velocity(self):
        """
        Get velocity from the GPS module.
        Wait for receiving about 10 values anc calculate average value.
        """
        vel_list = []
        sample_cnt = int(self.get_param_from_xml('GPS_SAMPLE_COUNT'))
        while len(vel_list) < sample_cnt:
            vel = self.parse_gps()
            if vel is not None:
                vel_list.append(vel)

        # remove max/min value from the list
        vel_list.sort()
        vel_list.pop(0)
        vel_list.pop(-1)

        # get average value in list
        avg_val = reduce(lambda x, y: x + y, vel_list) / len(vel_list)

        return avg_val

    def parse_gps(self):
        s_time = time.time()
        timeout = int(self.get_param_from_xml('GPS_TIMEOUT'))
        while time.time() - s_time < timeout:
            packet = self.serialPort.readline()
            # Packets which contains speed information is "RMC" & "VTG"
            if packet.find('RMC') > 0:
                l_val = packet.split(',')
                try:
                    vel_mps = float(l_val[7]) * 0.514
                    print "Velocity: %0.3f" % vel_mps, " M/s"
                    return vel_mps

                except ValueError as e:  # when there is no velocity data in the RMC packet.
                    continue
            elif packet.find('VTG') > 0:
                print "Getting velocity..."
            else:
                time.sleep(0.1)
        return None

    def move_blds(self, speed):
        # Move servo on channel between extremes.
        self.pwm.set_pwm(self.blds_channel, 0, speed)
        print "Setting BLDS width as ", speed
        time.sleep(1)

    def stop_blds(self):
        self.pwm.set_pwm(self.blds_channel, 0, 0)

    def move_servo(self, position):
        self.pwm.set_pwm(self.servo_channel, 0, position)


if __name__ == "__main__":

    ctrl = NavioCtrl()
    print ctrl.get_velocity()
