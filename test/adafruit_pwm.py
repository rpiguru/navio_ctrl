"""
Test code for controlling ESC with Adafruit PCA9685 board.

The frequency is set as 45.5Hz, but real frequency is 50Hz.(Measured with Oscilloscope)

- At first, give 500 that ESC detects the PWM signal from the RPi.
If ESC does not detect input signal, its LED will blink and otherwise LED will be turned off.
- Set value to drive brushless motor.
In my case, motor begins moving from 325.

"""

from __future__ import division

import time
# Import the PCA9685 module.
import Adafruit_PCA9685

# Initialise the PCA9685 using the default address (0x40).
pwm = Adafruit_PCA9685.PCA9685()


# pwm.set_pwm_freq(45.5)
pwm.set_pwm_freq(50)

print('Moving servo on channel 0, press Ctrl-C to quit..')

freq = raw_input("Please input frequency:")

# pwm.set_pwm_freq(45.5) - when brushless motor
pwm.set_pwm_freq(freq)

channel = raw_input("Please input channel number:")

while True:
    val = raw_input("Please input pulse width:")
    try:
        p_width = int(val)
    except ValueError:
        print "Input correct value."
        continue

    # Move servo on channel between extremes.
    pwm.set_pwm(int(channel), 0, p_width)
    print "Setting as ", p_width
    time.sleep(1)

