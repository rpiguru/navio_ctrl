
import serial
import time


def parseGPS(packet):
    s_time = time.time()
    # Packets which contains speed information is "RMC" & "VTG"
    if packet.find('RMC') > 0:
        l_val = packet.split(',')
        print ""

        try:
            vel_mps = float(l_val[7]) * 0.514
            print "Velocity: %0.3f" % vel_mps, " M/s"
            print "Elapsed: ", time.time() - s_time

        except ValueError as e:
            print "Not received yet..."


serialPort = serial.Serial("/dev/ttyAMA0", 9600, timeout=0.5)

while True:
    s = serialPort.readline()
    parseGPS(s)

